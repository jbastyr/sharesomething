﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using osu_common.Libraries.NetLib;
using System.Runtime.InteropServices;
using System.Diagnostics;
using sharesomething.Libraries;
using System.Threading;

namespace sharesomething
{
  public static class ScreenshotCapture
  {
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
      public int Left;
      public int Top;
      public int Right;
      public int Bottom;
    }

    [DllImport("dwmapi.dll", EntryPoint = "DwmIsCompositionEnabled")]
    public static extern int DwmIsCompositionEnabled(out bool enabled);

    internal static unsafe void Run(int minX, int maxX, int minY, int maxY)
    {
      try
      {
        int width = maxX - minX;
        int height = maxY - minY;

        using (Bitmap b = new Bitmap(width, height, PixelFormat.Format24bppRgb))
        using (Graphics g = Graphics.FromImage(b))
        {
          try
          {
            bool aero;
            DwmIsCompositionEnabled(out aero);
            if (!aero) Thread.Sleep(200);
          }
          catch { }

          g.CopyFromScreen(minX, minY, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);

          FileUpload.UploadImage(b, "ss");
        }
      }
      catch (Exception e)
      {
        TopMostMessageBox.Show("Failed to take screenshot.  Please report this if it should have worked!\n" + e.ToString(), "sharesomething", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }
  }
}
