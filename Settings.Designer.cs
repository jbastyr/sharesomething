﻿using sharesomething.Properties;
namespace sharesomething
{
  partial class Settings
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();

      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxStartup = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelSaveImage = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.textBoxSaveLocation = new System.Windows.Forms.TextBox();
            this.checkBoxSaveImage = new System.Windows.Forms.CheckBox();
            this.checkBoxClipboard = new System.Windows.Forms.CheckBox();
            this.checkBoxBrowser = new System.Windows.Forms.CheckBox();
            this.checkBoxSound = new System.Windows.Forms.CheckBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonToggleBinding = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.buttonUploadBinding = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonScreenSelectionBinding = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupAccountDetails = new System.Windows.Forms.GroupBox();
            this.displayDiskUsage = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.displayExpiryDate = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.displayAccountType = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.displayApiKey = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.displayUsername = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupLogin = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.checkBoxContextMenu = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.qualityHigh = new System.Windows.Forms.RadioButton();
            this.qualityBest = new System.Windows.Forms.RadioButton();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelSaveImage.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupAccountDetails.SuspendLayout();
            this.groupLogin.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(468, 335);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(460, 309);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton3);
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(6, 155);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(448, 117);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tray Icon Behaviour";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(36, 87);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(149, 19);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Open upload file dialog";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(36, 63);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(169, 19);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Begin screen capture mode";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(36, 39);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(134, 19);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Show settings dialog";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "On double-click...";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxStartup);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(448, 50);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "General Settings";
            // 
            // checkBoxStartup
            // 
            this.checkBoxStartup.AutoSize = true;
            this.checkBoxStartup.Location = new System.Drawing.Point(36, 21);
            this.checkBoxStartup.Name = "checkBoxStartup";
            this.checkBoxStartup.Size = new System.Drawing.Size(143, 19);
            this.checkBoxStartup.TabIndex = 3;
            this.checkBoxStartup.Text = "Start sharesomething on startup";
            this.checkBoxStartup.UseVisualStyleBackColor = true;
            this.checkBoxStartup.CheckedChanged += new System.EventHandler(this.checkBoxStartup_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panelSaveImage);
            this.groupBox1.Controls.Add(this.checkBoxSaveImage);
            this.groupBox1.Controls.Add(this.checkBoxClipboard);
            this.groupBox1.Controls.Add(this.checkBoxBrowser);
            this.groupBox1.Controls.Add(this.checkBoxSound);
            this.groupBox1.Location = new System.Drawing.Point(6, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(448, 93);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "On successful upload";
            // 
            // panelSaveImage
            // 
            this.panelSaveImage.Controls.Add(this.button4);
            this.panelSaveImage.Controls.Add(this.textBoxSaveLocation);
            this.panelSaveImage.Location = new System.Drawing.Point(243, 40);
            this.panelSaveImage.Name = "panelSaveImage";
            this.panelSaveImage.Size = new System.Drawing.Size(198, 28);
            this.panelSaveImage.TabIndex = 4;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(169, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(23, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBoxSaveLocation
            // 
            this.textBoxSaveLocation.Location = new System.Drawing.Point(3, 3);
            this.textBoxSaveLocation.Name = "textBoxSaveLocation";
            this.textBoxSaveLocation.Size = new System.Drawing.Size(160, 20);
            this.textBoxSaveLocation.TabIndex = 4;
            this.textBoxSaveLocation.Leave += new System.EventHandler(this.textBoxSaveLocation_Leave);
            // 
            // checkBoxSaveImage
            // 
            this.checkBoxSaveImage.AutoSize = true;
            this.checkBoxSaveImage.Location = new System.Drawing.Point(223, 20);
            this.checkBoxSaveImage.Name = "checkBoxSaveImage";
            this.checkBoxSaveImage.Size = new System.Drawing.Size(166, 19);
            this.checkBoxSaveImage.TabIndex = 3;
            this.checkBoxSaveImage.Text = "Save a local copy of image";
            this.checkBoxSaveImage.UseVisualStyleBackColor = true;
            this.checkBoxSaveImage.CheckedChanged += new System.EventHandler(this.checkBoxSaveImage_CheckedChanged);
            // 
            // checkBoxClipboard
            // 
            this.checkBoxClipboard.AutoSize = true;
            this.checkBoxClipboard.Location = new System.Drawing.Point(35, 41);
            this.checkBoxClipboard.Name = "checkBoxClipboard";
            this.checkBoxClipboard.Size = new System.Drawing.Size(143, 19);
            this.checkBoxClipboard.TabIndex = 2;
            this.checkBoxClipboard.Text = "Copy link to clipboard";
            this.checkBoxClipboard.UseVisualStyleBackColor = true;
            this.checkBoxClipboard.CheckedChanged += new System.EventHandler(this.checkBoxClipboard_CheckedChanged);
            // 
            // checkBoxBrowser
            // 
            this.checkBoxBrowser.AutoSize = true;
            this.checkBoxBrowser.Location = new System.Drawing.Point(35, 64);
            this.checkBoxBrowser.Name = "checkBoxBrowser";
            this.checkBoxBrowser.Size = new System.Drawing.Size(135, 19);
            this.checkBoxBrowser.TabIndex = 1;
            this.checkBoxBrowser.Text = "Open link in browser";
            this.checkBoxBrowser.UseVisualStyleBackColor = true;
            this.checkBoxBrowser.CheckedChanged += new System.EventHandler(this.checkBoxOpenInBrowser_CheckedChanged);
            // 
            // checkBoxSound
            // 
            this.checkBoxSound.AutoSize = true;
            this.checkBoxSound.Location = new System.Drawing.Point(35, 20);
            this.checkBoxSound.Name = "checkBoxSound";
            this.checkBoxSound.Size = new System.Drawing.Size(157, 19);
            this.checkBoxSound.TabIndex = 0;
            this.checkBoxSound.Text = "Play a notification sound";
            this.checkBoxSound.UseVisualStyleBackColor = true;
            this.checkBoxSound.CheckedChanged += new System.EventHandler(this.checkBoxSound_CheckedChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(460, 309);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Key Bindings";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonToggleBinding);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.buttonUploadBinding);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.buttonScreenSelectionBinding);
            this.groupBox4.Location = new System.Drawing.Point(5, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(448, 188);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Keyboard Bindings";
            // 
            // buttonToggleBinding
            // 
            this.buttonToggleBinding.Location = new System.Drawing.Point(211, 89);
            this.buttonToggleBinding.Name = "buttonToggleBinding";
            this.buttonToggleBinding.Size = new System.Drawing.Size(150, 23);
            this.buttonToggleBinding.TabIndex = 20;
            this.buttonToggleBinding.UseVisualStyleBackColor = true;
            this.buttonToggleBinding.Click += new System.EventHandler(this.buttonToggleBinding_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(49, 93);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(151, 15);
            this.label15.TabIndex = 19;
            this.label15.Text = "Toggle keybind functionality:";
            // 
            // buttonUploadBinding
            // 
            this.buttonUploadBinding.Location = new System.Drawing.Point(211, 57);
            this.buttonUploadBinding.Name = "buttonUploadBinding";
            this.buttonUploadBinding.Size = new System.Drawing.Size(150, 23);
            this.buttonUploadBinding.TabIndex = 15;
            this.buttonUploadBinding.UseVisualStyleBackColor = true;
            this.buttonUploadBinding.Click += new System.EventHandler(this.buttonUploadBinding_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(49, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 15);
            this.label10.TabIndex = 14;
            this.label10.Text = "Upload File:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(49, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 15);
            this.label8.TabIndex = 11;
            this.label8.Text = "Capture Area:";
            // 
            // buttonScreenSelectionBinding
            // 
            this.buttonScreenSelectionBinding.Location = new System.Drawing.Point(211, 28);
            this.buttonScreenSelectionBinding.Name = "buttonScreenSelectionBinding";
            this.buttonScreenSelectionBinding.Size = new System.Drawing.Size(150, 23);
            this.buttonScreenSelectionBinding.TabIndex = 10;
            this.buttonScreenSelectionBinding.UseVisualStyleBackColor = true;
            this.buttonScreenSelectionBinding.Click += new System.EventHandler(this.buttonScreenSelection_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupAccountDetails);
            this.tabPage2.Controls.Add(this.groupLogin);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(460, 309);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Account";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupAccountDetails
            // 
            this.groupAccountDetails.Controls.Add(this.displayDiskUsage);
            this.groupAccountDetails.Controls.Add(this.label18);
            this.groupAccountDetails.Controls.Add(this.displayExpiryDate);
            this.groupAccountDetails.Controls.Add(this.label16);
            this.groupAccountDetails.Controls.Add(this.displayAccountType);
            this.groupAccountDetails.Controls.Add(this.label14);
            this.groupAccountDetails.Controls.Add(this.displayApiKey);
            this.groupAccountDetails.Controls.Add(this.label6);
            this.groupAccountDetails.Controls.Add(this.button1);
            this.groupAccountDetails.Controls.Add(this.buttonLogout);
            this.groupAccountDetails.Controls.Add(this.displayUsername);
            this.groupAccountDetails.Controls.Add(this.label1);
            this.groupAccountDetails.Location = new System.Drawing.Point(6, 6);
            this.groupAccountDetails.Name = "groupAccountDetails";
            this.groupAccountDetails.Size = new System.Drawing.Size(448, 200);
            this.groupAccountDetails.TabIndex = 2;
            this.groupAccountDetails.TabStop = false;
            this.groupAccountDetails.Text = "Account Details";
            // 
            // displayDiskUsage
            // 
            this.displayDiskUsage.AutoSize = true;
            this.displayDiskUsage.Location = new System.Drawing.Point(177, 104);
            this.displayDiskUsage.Name = "displayDiskUsage";
            this.displayDiskUsage.Size = new System.Drawing.Size(0, 15);
            this.displayDiskUsage.TabIndex = 11;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(40, 104);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(128, 17);
            this.label18.TabIndex = 10;
            this.label18.Text = "Disk Usage:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // displayExpiryDate
            // 
            this.displayExpiryDate.AutoSize = true;
            this.displayExpiryDate.Location = new System.Drawing.Point(177, 84);
            this.displayExpiryDate.Name = "displayExpiryDate";
            this.displayExpiryDate.Size = new System.Drawing.Size(0, 15);
            this.displayExpiryDate.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(40, 84);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(128, 17);
            this.label16.TabIndex = 8;
            this.label16.Text = "Expiry Date:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // displayAccountType
            // 
            this.displayAccountType.AutoSize = true;
            this.displayAccountType.Location = new System.Drawing.Point(177, 64);
            this.displayAccountType.Name = "displayAccountType";
            this.displayAccountType.Size = new System.Drawing.Size(0, 15);
            this.displayAccountType.TabIndex = 7;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(40, 64);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(128, 17);
            this.label14.TabIndex = 6;
            this.label14.Text = "Account Type:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // displayApiKey
            // 
            this.displayApiKey.AutoSize = true;
            this.displayApiKey.Location = new System.Drawing.Point(177, 44);
            this.displayApiKey.Name = "displayApiKey";
            this.displayApiKey.Size = new System.Drawing.Size(0, 15);
            this.displayApiKey.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(40, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "API Key:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(281, 38);
            this.button1.TabIndex = 3;
            this.button1.Text = "My Account";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonLogout
            // 
            this.buttonLogout.Location = new System.Drawing.Point(302, 150);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(128, 38);
            this.buttonLogout.TabIndex = 2;
            this.buttonLogout.Text = "Logout";
            this.buttonLogout.UseVisualStyleBackColor = true;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // displayUsername
            // 
            this.displayUsername.AutoSize = true;
            this.displayUsername.Location = new System.Drawing.Point(177, 24);
            this.displayUsername.Name = "displayUsername";
            this.displayUsername.Size = new System.Drawing.Size(0, 15);
            this.displayUsername.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(40, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Logged in as:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupLogin
            // 
            this.groupLogin.Controls.Add(this.label11);
            this.groupLogin.Controls.Add(this.textBoxPassword);
            this.groupLogin.Controls.Add(this.textBoxUsername);
            this.groupLogin.Controls.Add(this.label4);
            this.groupLogin.Controls.Add(this.buttonLogin);
            this.groupLogin.Controls.Add(this.label2);
            this.groupLogin.Controls.Add(this.label3);
            this.groupLogin.Location = new System.Drawing.Point(6, 6);
            this.groupLogin.Name = "groupLogin";
            this.groupLogin.Size = new System.Drawing.Size(448, 176);
            this.groupLogin.TabIndex = 3;
            this.groupLogin.TabStop = false;
            this.groupLogin.Text = "Account Setup";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(9, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(433, 40);
            this.label11.TabIndex = 5;
            this.label11.Text = "You need to login before you can make full use of sharesomething.";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.AcceptsReturn = true;
            this.textBoxPassword.Location = new System.Drawing.Point(138, 94);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(149, 20);
            this.textBoxPassword.TabIndex = 1;
            this.textBoxPassword.UseSystemPasswordChar = true;
            this.textBoxPassword.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            this.textBoxPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxUsername_KeyDown);
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.AcceptsReturn = true;
            this.textBoxUsername.Location = new System.Drawing.Point(138, 66);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(149, 20);
            this.textBoxUsername.TabIndex = 0;
            this.textBoxUsername.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            this.textBoxUsername.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxUsername_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(76, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Password:";
            // 
            // buttonLogin
            // 
            this.buttonLogin.Enabled = false;
            this.buttonLogin.Location = new System.Drawing.Point(308, 66);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(127, 48);
            this.buttonLogin.TabIndex = 2;
            this.buttonLogin.Text = "Login";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(174, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 15);
            this.label2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(97, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Email:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox6);
            this.tabPage5.Controls.Add(this.groupBox5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(460, 309);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Advanced";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.checkBoxContextMenu);
            this.groupBox6.Location = new System.Drawing.Point(6, 77);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(448, 52);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Context Menu";
            // 
            // checkBoxContextMenu
            // 
            this.checkBoxContextMenu.AutoSize = true;
            this.checkBoxContextMenu.Location = new System.Drawing.Point(16, 21);
            this.checkBoxContextMenu.Name = "checkBoxContextMenu";
            this.checkBoxContextMenu.Size = new System.Drawing.Size(205, 19);
            this.checkBoxContextMenu.TabIndex = 0;
            this.checkBoxContextMenu.Text = "Show explorer context menu item";
            this.checkBoxContextMenu.UseVisualStyleBackColor = true;
            this.checkBoxContextMenu.CheckedChanged += new System.EventHandler(this.checkBoxContextMenu_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.qualityHigh);
            this.groupBox5.Controls.Add(this.qualityBest);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(448, 70);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Screen Capture Quality";
            // 
            // qualityHigh
            // 
            this.qualityHigh.AutoSize = true;
            this.qualityHigh.Location = new System.Drawing.Point(17, 44);
            this.qualityHigh.Name = "qualityHigh";
            this.qualityHigh.Size = new System.Drawing.Size(273, 19);
            this.qualityHigh.TabIndex = 5;
            this.qualityHigh.TabStop = true;
            this.qualityHigh.Text = "Smart (use JPG unless PNG is smaller in filesize)";
            this.qualityHigh.UseVisualStyleBackColor = true;
            this.qualityHigh.CheckedChanged += new System.EventHandler(this.qualityHigh_CheckedChanged);
            // 
            // qualityBest
            // 
            this.qualityBest.AutoSize = true;
            this.qualityBest.Location = new System.Drawing.Point(17, 20);
            this.qualityBest.Name = "qualityBest";
            this.qualityBest.Size = new System.Drawing.Size(187, 19);
            this.qualityBest.TabIndex = 4;
            this.qualityBest.TabStop = true;
            this.qualityBest.Text = "No Compression (always PNG)";
            this.qualityBest.UseVisualStyleBackColor = true;
            this.qualityBest.CheckedChanged += new System.EventHandler(this.qualityBest_CheckedChanged);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 359);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::sharesomething.Properties.Resources.iconbundle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "sharesomething settings";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Settings_KeyDown);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelSaveImage.ResumeLayout(false);
            this.panelSaveImage.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupAccountDetails.ResumeLayout(false);
            this.groupAccountDetails.PerformLayout();
            this.groupLogin.ResumeLayout(false);
            this.groupLogin.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

    }

    #endregion

    internal System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.CheckBox checkBoxBrowser;
    private System.Windows.Forms.CheckBox checkBoxSound;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.GroupBox groupAccountDetails;
    private System.Windows.Forms.Button buttonLogout;
    private System.Windows.Forms.Label displayUsername;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox groupLogin;
    private System.Windows.Forms.TextBox textBoxUsername;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Button buttonLogin;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox textBoxPassword;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.CheckBox checkBoxClipboard;
    private System.Windows.Forms.Label displayApiKey;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.CheckBox checkBoxStartup;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.RadioButton radioButton3;
    private System.Windows.Forms.RadioButton radioButton2;
    private System.Windows.Forms.RadioButton radioButton1;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Button button4;
    private System.Windows.Forms.TextBox textBoxSaveLocation;
    private System.Windows.Forms.CheckBox checkBoxSaveImage;
    private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    private System.Windows.Forms.Panel panelSaveImage;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TabPage tabPage4;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.Button buttonUploadBinding;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Button buttonScreenSelectionBinding;
    private System.Windows.Forms.Label displayDiskUsage;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Label displayExpiryDate;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.Label displayAccountType;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Button buttonToggleBinding;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TabPage tabPage5;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.RadioButton qualityHigh;
    private System.Windows.Forms.RadioButton qualityBest;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.CheckBox checkBoxContextMenu;
  }
}