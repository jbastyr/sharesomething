﻿using System;
using System.Windows.Forms;
using osu_common.Libraries.NetLib;
using System.Diagnostics;
using osu_common.Helpers;
using System.IO;

namespace sharesomething
{
  enum UploadQuality
  {
    Best,
    High,
    Medium
  }

  enum DoubleClickBehaviour
  {
    OpenSettings,
    ScreenSelect,
    UploadFile
  }

  public partial class Settings : pForm
  {
    public static void ShowPreferences()
    {
      MainForm.Instance.Invoke(delegate
      {
        if (Instance == null)
          Instance = new Settings();

        Instance.Show();
        Instance.Focus();
        Instance.BringToFront();
        Instance.TopMost = true;
        Instance.TopMost = false;
      });
    }

    internal static Settings Instance;

    protected override void OnShown(EventArgs e)
    {
      UpdateAccountDetails();

      base.OnShown(e);
    }

    protected override void OnClosed(EventArgs e)
    {
      Instance = null;

      EndKeyCapture();

      CheckSaveLocation();
      sharesomething.config.SetValue<string>("saveimagepath", textBoxSaveLocation.Text);

      base.OnClosed(e);

      this.Dispose();
    }

    public Settings()
    {
      InitializeComponent();

      ReloadConfig();
    }

    /// <summary>
    /// Some toggles in reloading config cause events to be triggered.  We don't want to end up reloading more than we need to.
    /// </summary>
    bool isReloading;

    internal void ReloadConfig()
    {
      if (isReloading) return;
      isReloading = true;

      Invoke(delegate
      {
        bool isLoggedIn = sharesomething.IsLoggedIn;

        displayUsername.Text = sharesomething.config.GetValue<string>("username", null);
        displayApiKey.Text = sharesomething.config.GetValue<string>("key", null);

        int accountTypeId = sharesomething.config.GetValue<int>("type", 0);
        long usage = sharesomething.config.GetValue<long>("usage", 0);

        displayAccountType.Text = sharesomething.GetAccountTypeString(accountTypeId);
        displayDiskUsage.Text = accountTypeId == 0 ? string.Format("{0}/200mb", usage / 1048576) : string.Format("{0}mb", usage / 1048576);
        displayExpiryDate.Text = sharesomething.config.GetValue<string>("expiry", "Unknown");

        groupAccountDetails.Visible = isLoggedIn;
        groupLogin.Visible = !isLoggedIn;

        checkBoxBrowser.Checked = sharesomething.config.GetValue<bool>("openbrowser", false);
        checkBoxSound.Checked = sharesomething.config.GetValue<bool>("notificationsound", true);
        checkBoxClipboard.Checked = sharesomething.config.GetValue<bool>("copytoclipboard", true);
        checkBoxStartup.Checked = sharesomething.config.GetValue<bool>("startup", true);

        checkBoxContextMenu.Checked = sharesomething.config.GetValue<bool>("contextmenu", true);

        checkBoxSaveImage.Checked = sharesomething.config.GetValue<bool>("saveimages", false);
        textBoxSaveLocation.Text = sharesomething.config.GetValue<string>("saveimagepath", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
        panelSaveImage.Enabled = checkBoxSaveImage.Checked;

        folderBrowserDialog1.ShowNewFolderButton = true;
        folderBrowserDialog1.SelectedPath = textBoxSaveLocation.Text;

        buttonScreenSelectionBinding.Text = BindingManager.GetStringRepresentationFor(KeyBinding.ScreenSelection);
        buttonUploadBinding.Text = BindingManager.GetStringRepresentationFor(KeyBinding.UploadFile);
        buttonToggleBinding.Text = BindingManager.GetStringRepresentationFor(KeyBinding.Toggle);

        DoubleClickBehaviour doubleClickBehaviour = (DoubleClickBehaviour)sharesomething.config.GetValue<int>("doubleclickbehaviour", 0);

        radioButton1.Checked = doubleClickBehaviour == DoubleClickBehaviour.OpenSettings;
        radioButton2.Checked = doubleClickBehaviour == DoubleClickBehaviour.ScreenSelect;
        radioButton3.Checked = doubleClickBehaviour == DoubleClickBehaviour.UploadFile;

        UploadQuality quality = (UploadQuality)sharesomething.config.GetValue<int>("uploadquality", 1);

        qualityBest.Checked = quality == UploadQuality.Best;
        qualityHigh.Checked = quality == UploadQuality.High;

        isReloading = false;
      });
    }

    private void buttonLogout_Click(object sender, EventArgs e)
    {
      sharesomething.Logout();

      textBoxPassword.Text = "";
    }

    private void buttonLogin_Click(object sender, EventArgs e)
    {
      FormNetRequest loginRequest = new FormNetRequest(sharesomething.getApiUrl("auth"));
      loginRequest.AddField("e", textBoxUsername.Text);
      loginRequest.AddField("p", textBoxPassword.Text);
      loginRequest.AddField("z", "poop");
      loginRequest.onFinish += new FormNetRequest.RequestCompleteHandler(login_onFinish);
      NetManager.AddRequest(loginRequest);

      groupLogin.Enabled = false;
    }

    internal static void UpdateAccountDetails()
    {
      if (sharesomething.IsLoggedIn)
      {
        FormNetRequest loginRequest = new FormNetRequest(sharesomething.getApiUrl("auth"));
        loginRequest.AddField("e", sharesomething.config.GetValue<string>("username", ""));
        loginRequest.AddField("k", sharesomething.config.GetValue<string>("key", ""));
        loginRequest.AddField("z", "poop");
        loginRequest.onFinish += login_onFinish;
        NetManager.AddRequest(loginRequest);
      }
    }

    static void login_onFinish(string _result, Exception e)
    {
      int status = -1;

      string[] split = null;

      try
      {
        split = _result.Split(',');
        status = Int32.Parse(split[0]);
      }
      catch
      {
        if (Settings.Instance != null)
          sharesomething.ShowErrorBalloon("Connection with server went wrong.  Please check your connection and try again.", "Authentication Failure");
        return;
      }

      if (status < 0)
      {
        sharesomething.ShowErrorBalloon("The username or password you entered is incorrect.", "Authentication Failure");
        sharesomething.config.SetValue<string>("key", null);
      }
      else
      {
        int accountTypeId = status;

        string expiry = split[2];

        if (expiry.Length == 0)
          expiry = "Never";

        long usage = Int64.Parse(split[3]);

        sharesomething.config.SetValue<string>("key", split[1]);
        sharesomething.config.SetValue<int>("type", accountTypeId);
        sharesomething.config.SetValue<long>("usage", usage);
        sharesomething.config.SetValue<string>("expiry", expiry);

        HistoryManager.Update();
      }

      if (Settings.Instance != null)
        Settings.Instance.UpdateAfterLogin();
    }

    private void UpdateAfterLogin()
    {
      Invoke((MethodInvoker)delegate
      {
        if (textBoxUsername.Text.Length > 0)
          sharesomething.config.SetValue<string>("username", textBoxUsername.Text);

        groupLogin.Enabled = true;

        ReloadConfig();
      });
    }

    private void textBox_TextChanged(object sender, EventArgs e)
    {
      UpdateLoginButton();
    }

    private void UpdateLoginButton()
    {
      buttonLogin.Enabled = textBoxUsername.TextLength > 0 && textBoxPassword.TextLength > 0;
    }

    private void textBoxUsername_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        buttonLogin.PerformClick();
        e.Handled = true;
      }
    }

    private void checkBoxSound_CheckedChanged(object sender, EventArgs e)
    {
      sharesomething.config.SetValue<bool>("notificationsound", checkBoxSound.Checked);
    }

    private void checkBoxOpenInBrowser_CheckedChanged(object sender, EventArgs e)
    {
      sharesomething.config.SetValue<bool>("openbrowser", checkBoxBrowser.Checked);
    }

    private void checkBoxClipboard_CheckedChanged(object sender, EventArgs e)
    {
      sharesomething.config.SetValue<bool>("copytoclipboard", checkBoxClipboard.Checked);
    }

    private void button1_Click(object sender, EventArgs e)
    {
      sharesomething.ViewAccount();
    }

    private void checkBoxStartup_CheckedChanged(object sender, EventArgs e)
    {
      sharesomething.config.SetValue<bool>("startup", checkBoxStartup.Checked);

      sharesomething.SetStartupBehaviour();
    }

    private void checkBoxSaveImage_CheckedChanged(object sender, EventArgs e)
    {
      sharesomething.config.SetValue<bool>("saveimages", checkBoxSaveImage.Checked);

      ReloadConfig();
    }

    private void button4_Click(object sender, EventArgs e)
    {
      if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
      {
        sharesomething.config.SetValue<string>("saveimagepath", folderBrowserDialog1.SelectedPath);
        ReloadConfig();
      }
    }

    private void radioButton1_CheckedChanged(object sender, EventArgs e)
    {
      if (radioButton1.Checked)
      {
        sharesomething.config.SetValue<int>("doubleclickbehaviour", 0);
        ReloadConfig();
      }
    }

    private void radioButton2_CheckedChanged(object sender, EventArgs e)
    {
      if (radioButton2.Checked)
      {
        sharesomething.config.SetValue<int>("doubleclickbehaviour", 1);
        ReloadConfig();
      }
    }

    private void radioButton3_CheckedChanged(object sender, EventArgs e)
    {
      if (radioButton3.Checked)
      {
        sharesomething.config.SetValue<int>("doubleclickbehaviour", 2);
        ReloadConfig();
      }
    }

    private void Settings_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Escape)
        Close();
    }

    private void buttonScreenSelection_Click(object sender, EventArgs e)
    {
      StartKeyCapture(buttonScreenSelectionBinding, KeyBinding.ScreenSelection);
    }

    private void buttonUploadBinding_Click(object sender, EventArgs e)
    {
      StartKeyCapture(buttonUploadBinding, KeyBinding.UploadFile);
    }

    private void textBoxSaveLocation_Leave(object sender, EventArgs e)
    {
      CheckSaveLocation();
    }

    private void CheckSaveLocation()
    {
      if (!Directory.Exists(textBoxSaveLocation.Text))
        textBoxSaveLocation.Text = sharesomething.config.GetValue<string>("saveimagepath", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
    }

    private void buttonToggleBinding_Click(object sender, EventArgs e)
    {
      StartKeyCapture(buttonToggleBinding, KeyBinding.Toggle);
    }

    private void qualityBest_CheckedChanged(object sender, EventArgs e)
    {
      if (qualityBest.Checked)
        sharesomething.config.SetValue<int>("uploadquality", (int)UploadQuality.Best);
    }

    private void qualityHigh_CheckedChanged(object sender, EventArgs e)
    {
      if (qualityHigh.Checked)
        sharesomething.config.SetValue<int>("uploadquality", (int)UploadQuality.High);
    }

    private void checkBoxContextMenu_CheckedChanged(object sender, EventArgs e)
    {
      sharesomething.config.SetValue<bool>("contextmenu", checkBoxContextMenu.Checked);
      if (checkBoxContextMenu.Checked)
        ContextMenuHandler.Install();
      else
        ContextMenuHandler.Remove();
    }
  }
}
