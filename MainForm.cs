﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Gma.UserActivityMonitor;
using System.Diagnostics;
using System.Runtime.InteropServices;
using osu_common.Helpers;
using System.IO;
using sharesomething.Properties;
using System.Threading;
using sharesomething.Libraries;
using System.Collections.Specialized;

namespace sharesomething
{
  public partial class MainForm : pForm
  {
    public static MainForm Instance;

    public MainForm()
    {
      InitializeComponent();
      Instance = this;

      Visible = false;
      Hide();

      //TopMost = true;
      Icon = Resources.iconbundle;

      trayIcon.BalloonTipClicked += new EventHandler(trayIcon_BalloonTipClicked);

      toolStripMenuItemUploadDisabled.Checked = sharesomething.config.GetValue<bool>("disableupload", false);

      toolStripMenuItemVersion.Text = string.Format("sharesomething r{0}", sharesomething.INTERNAL_VERSION);

      BindingManager.Bind();

      if (sharesomething.IsLoggedIn) {
        Settings.UpdateAccountDetails();
      }

      HistoryManager.Update();

      if (sharesomething.RecoveringFromError)
        sharesomething.ShowErrorBalloon("This has been reported automatically.\nSorry for any inconvenience", "sharesomething has just recovered from an error.");

    }

    internal static List<Keys> PressedKeys = new List<Keys>();

    void HookManager_KeyUp(object sender, KeyEventArgs e)
    {
      PressedKeys.Remove(e.KeyData);
      handledKeyDownCount = PressedKeys.Count;
    }

    static int handledKeyDownCount;
    void HookManager_KeyDown(object sender, KeyEventArgs e)
    {
      if (Settings.IsKeyCapturing || sharesomething.config.GetValue<bool>("disableupload", false)) return;

      e.Handled = true;

      if (captureMode && e.KeyCode == Keys.Escape)
      {
        EndSelection();
        return;
      }

      if (PressedKeys.Contains(e.KeyCode))
      {
        e.Handled = false;
        return;
      }

      PressedKeys.Add(e.KeyCode);

      e.Handled = false;
    }

    internal static void threadMeSome(MethodInvoker method)
    {
      if (method == null) return;

      try
      {
        Thread t = new Thread(() => method());
        t.IsBackground = true;
        t.Start();
      }
      catch
      {
        //do something here too..  
      }

    }

    internal static void invokeMeSome(MethodInvoker method)
    {
      if (method == null) return;
      try
      {
        Thread t = new Thread((ParameterizedThreadStart)delegate
        {
          try
          {
            Instance.Invoke(delegate
                    {
                  method();
                });
          }
          catch
          {
            //do something here?!
          }
        });
        t.IsBackground = true;
        t.Start();
      }
      catch
      {

      }

    }

    void trayIcon_BalloonTipClicked(object sender, EventArgs e)
    {
      try
      {
        string tag = trayIcon.Tag as string;

        if (tag == null) return;

        if (tag.StartsWith("http") && !sharesomething.config.GetValue<bool>("openbrowser", false))
          Process.Start("http://" + tag.Replace("http://", ""));
      }
      catch { }
    }

    private void toolStripMenuItem1_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }

    void toolStripMenuItem2_Click(object sender, System.EventArgs e)
    {
      Settings.ShowPreferences();
    }

    private void toolStripMenuItem5_Click(object sender, EventArgs e)
    {
      sharesomething.ViewAccount();
    }

    void toolStripMenuItemSelection_Click(object sender, System.EventArgs e)
    {
      StartSelection();
    }

    void toolStripMenuItemUploadFile_Click(object sender, System.EventArgs e)
    {
      UploadFile();
    }

    void toolStripMenuItem3_Click(object sender, System.EventArgs e)
    {
      Toggle();
    }

    internal static void Toggle()
    {
      invokeMeSome(delegate
                       {
                         Instance.toolStripMenuItemUploadDisabled.Checked =
                                   !Instance.toolStripMenuItemUploadDisabled.Checked;
                         sharesomething.config.SetValue<bool>("disableupload", Instance.toolStripMenuItemUploadDisabled.Checked);

                         if (Instance.toolStripMenuItemUploadDisabled.Checked)
                         {
                           MainForm.Instance.trayIcon.ShowBalloonTip(2000, "sharesomething was disabled!", "Shortcut keys will no longer be accepted.", ToolTipIcon.Info);
                           MainForm.Instance.trayIcon.Tag = null;
                         }
                         else
                         {
                           MainForm.Instance.trayIcon.ShowBalloonTip(2000, "sharesomething was enabled!", "Shortcut keys will now be accepted.", ToolTipIcon.Info);
                           MainForm.Instance.trayIcon.Tag = null;
                         }


                       });
    }

    private void trayIcon_MouseClick(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Left)
      {
        if (FileUpload.CurrentUpload != null)
          if (TopMostMessageBox.Show("Would you like to cancel the current upload?", "sharesomething", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == System.Windows.Forms.DialogResult.Yes)
            FileUpload.CancelCurrent();
      }
    }

    private void trayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Left)
      {
        switch ((DoubleClickBehaviour)sharesomething.config.GetValue<int>("doubleclickbehaviour", 0))
        {
          case DoubleClickBehaviour.OpenSettings:
            Settings.ShowPreferences();
            break;
          case DoubleClickBehaviour.ScreenSelect:
            StartSelection();
            break;
          case DoubleClickBehaviour.UploadFile:
            UploadFile();
            break;
        }
      }
    }

    /// <summary>The GetForegroundWindow function returns a handle to the foreground window.</summary>
    [DllImport("user32.dll")]
    static extern IntPtr GetForegroundWindow();

    [DllImport("user32.dll", SetLastError = true)]
    static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    private struct BITMAPFILEHEADER
    {
      public static readonly short BM = 0x4d42; // BM

      public short bfType;
      public int bfSize;
      public short bfReserved1;
      public short bfReserved2;
      public int bfOffBits;
    }

    [StructLayout(LayoutKind.Sequential)]
    private struct BITMAPINFOHEADER
    {
      public int biSize;
      public int biWidth;
      public int biHeight;
      public short biPlanes;
      public short biBitCount;
      public int biCompression;
      public int biSizeImage;
      public int biXPelsPerMeter;
      public int biYPelsPerMeter;
      public int biClrUsed;
      public int biClrImportant;
    }

    public static class BinaryStructConverter
    {
      public static T FromByteArray<T>(byte[] bytes) where T : struct
      {
        IntPtr ptr = IntPtr.Zero;
        try
        {
          int size = Marshal.SizeOf(typeof(T));
          ptr = Marshal.AllocHGlobal(size);
          Marshal.Copy(bytes, 0, ptr, size);
          object obj = Marshal.PtrToStructure(ptr, typeof(T));
          return (T)obj;
        }
        finally
        {
          if (ptr != IntPtr.Zero)
            Marshal.FreeHGlobal(ptr);
        }
      }

      public static byte[] ToByteArray<T>(T obj) where T : struct
      {
        IntPtr ptr = IntPtr.Zero;
        try
        {
          int size = Marshal.SizeOf(typeof(T));
          ptr = Marshal.AllocHGlobal(size);
          Marshal.StructureToPtr(obj, ptr, true);
          byte[] bytes = new byte[size];
          Marshal.Copy(ptr, bytes, 0, size);
          return bytes;
        }
        finally
        {
          if (ptr != IntPtr.Zero)
            Marshal.FreeHGlobal(ptr);
        }
      }
    }

    static bool uploadFileDialogVisible;

    private void UploadFile()
    {
      if (!sharesomething.EnsureLogin()) return;

      if (uploadFileDialogVisible) return;
      uploadFileDialogVisible = true;

      //check is explorer is active

      IntPtr windowPtr = GetForegroundWindow();

      uint processId = 0;
      GetWindowThreadProcessId(windowPtr, out processId);

      Process activeWindow = Process.GetProcessById((int)processId);

      if (activeWindow.ProcessName == "explorer")
      {
        while (PressedKeys.Count > 0)
          Thread.Sleep(100);

        bool didUpload = false;

        Invoke(delegate
        {
          try
          {
            //store previous clipboard contents...
            IDataObject lastContents = Clipboard.GetDataObject();
            Clipboard.Clear();

            SendKeys.SendWait("^c");

            if (Clipboard.ContainsFileDropList())
            {
              StringCollection selectedFiles = Clipboard.GetFileDropList();
              string list = "";
              foreach (string s in selectedFiles)
                list += s + "\n";

              if (TopMostMessageBox.Show(list + "upload " + (selectedFiles.Count > 1 ? "these files?" : "this file?"), "sharesomething", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
              {
                foreach (string file in selectedFiles)
                  FileUpload.Upload(file);
              }

              uploadFileDialogVisible = false;
              didUpload = true;
            }

            //restore contents...
            Clipboard.SetDataObject(lastContents);
          }
          catch
          {
            //likely a clipboard error
          }
        }, true);

        if (didUpload) return;
      }

      bool wasSuccessful = false;

      Invoke(delegate
          {
            wasSuccessful = openFileDialog1.ShowDialog(this) == DialogResult.OK && !string.IsNullOrEmpty(openFileDialog1.FileName);
          });

      if (wasSuccessful)
      {
        foreach (String file in openFileDialog1.FileNames)
        {
          FileUpload.Upload(file);
        }
      }

      //topmostForm.Dispose();
      uploadFileDialogVisible = false;
    }

    protected override CreateParams CreateParams
    {
      get
      {
        CreateParams cp = base.CreateParams;
        // turn on WS_EX_TOOLWINDOW style bit
        cp.ExStyle |= 0x80;
        return cp;
      }
    }

    private void trayResetTimer_Tick(object sender, EventArgs e)
    {
      System.Windows.Forms.Timer t = sender as System.Windows.Forms.Timer;
      t.Stop();

      SetTrayIcon(Resources.tray, "sharesomething");
    }

    private void UpdateArbitraryPosition(Point location)
    {
      if (selectionForm == null || !updatePosition) return;

      selectionForm.Location = new Point(location.X - 4, location.Y - 4);
      selectionForm.Size = new Size(8, 8);
      selectionForm.Opacity = 0.01;
    }

    internal static void SetTrayIcon(Icon icon, string tooltip)
    {
      SetTrayIcon(icon, tooltip, 0);
    }

    internal static void SetTrayIcon(Icon icon, string tooltip, int timeToReset)
    {
      Instance.Invoke(delegate
      {
        if (Instance.trayIcon.Icon != null)
          Instance.trayIcon.Icon.Dispose();
        Instance.trayIcon.Icon = icon;
        Instance.trayIcon.Text = tooltip;

        if (timeToReset > 0)
        {
          Instance.trayResetTimer.Interval = timeToReset;
          Instance.trayResetTimer.Start();
        }
      });
    }


    void toolStripMenuItemCancelUpload_Click(object sender, System.EventArgs e)
    {
      FileUpload.CancelCurrent();
    }

    protected override void WndProc(ref Message m)
    {
      const int WM_HOTKEY = 0x0312;

      switch (m.Msg)
      {
        case WM_HOTKEY:
          {
            switch (BindingManager.Check((short)m.WParam))
            {
              case KeyBinding.ScreenSelection:
                PressedKeys.Clear();
                if (sharesomething.config.GetValue<bool>("disableupload", false))
                  return;
                invokeMeSome(StartSelection);
                return;
              case KeyBinding.UploadFile:
                PressedKeys.Clear();
                if (sharesomething.config.GetValue<bool>("disableupload", false))
                  return;
                threadMeSome(UploadFile);
                return;
              case KeyBinding.Toggle:
                Toggle();
                return;
            }

            break;
          }
        default:
          {
            base.WndProc(ref m);
            break;
          }
      }
    }
  }
}
