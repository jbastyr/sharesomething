﻿using System;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Remoting;

namespace sharesomething
{
  public class IpcLoader : MarshalByRefObject
  {
    public void upload(string filename)
    {
      FileUpload.Upload(filename);
    }
  }

  internal static class IPC
  {
    internal static IpcChannel Channel;
    internal static void AcceptConnections()
    {
      bool accepted = acceptConnections();

      if (accepted)
        return;

      //kill other instances?
      return;
    }

    internal static void Unregister()
    {
      if (Channel != null)
        ChannelServices.UnregisterChannel(Channel);
    }

    private static bool acceptConnections()
    {
      try
      {
        Channel = new IpcChannel("sharesomething");

        ChannelServices.RegisterChannel(Channel, false);
        RemotingConfiguration.RegisterWellKnownServiceType(typeof(IpcLoader), "sharesomething", WellKnownObjectMode.Singleton);

        return true;
      }
      catch
      {
        Channel = null;
      }

      return false;
    }

    internal static void LoadFile(string file)
    {
      IpcChannel ipcCh = new IpcChannel("sharesomething-incoming");
      ChannelServices.RegisterChannel(ipcCh, false);

      IpcLoader obj =
          (IpcLoader)Activator.GetObject
                          (typeof(IpcLoader), "ipc://sharesomething/sharesomething");
      obj.upload(file);

      ChannelServices.UnregisterChannel(ipcCh);
    }
  }
}
