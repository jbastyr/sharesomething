﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace osu_common.Helpers
{
  public class pForm : Form
  {
    protected override void OnLoad(EventArgs e)
    {
      FixFonts();

      base.OnLoad(e);
    }

    private void FixFonts()
    {
      AutoScaleMode = AutoScaleMode.None;

      foreach (Control c in Controls)
        c.Font = Font;
    }

    public override Font Font
    {
      get
      {
        return SystemFonts.MessageBoxFont; //should be SegoiUI
      }
      set
      {
        base.Font = value;
      }
    }

    public void Invoke(MethodInvoker moo)
    {
      Invoke(moo, false);
    }

    public void Invoke(MethodInvoker moo, bool force)
    {
      try
      {
        if (InvokeRequired)
        {
          if (!base.IsHandleCreated)
          {
            Thread.Sleep(3000);
            if (!base.IsHandleCreated)
              return;
          }

          if (Disposing || IsDisposed)
            return;

          int tryCount = 5;
          while (tryCount-- > 0)
          {
            if (IsHandleCreated)
            {
              base.Invoke(moo);
              break;
            }
            else
              Thread.Sleep(600);
          }
        }
        else
          moo();
      }
      catch
      { }
    }
  }
}